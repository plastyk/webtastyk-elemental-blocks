<?php

namespace Plastyk\Elemental\Control;

use DNADesign\Elemental\Controllers\ElementController;
use SilverStripe\View\Requirements;

class MediaGalleryElementController extends ElementController
{
    protected function init()
    {
        parent::init();

        Requirements::css('plastyk/webtastyk-elemental-blocks:css/magnific-popup.css');
        Requirements::javascript('plastyk/webtastyk-elemental-blocks:javascript/jquery.magnific-popup.min.js');
        Requirements::javascript('plastyk/webtastyk-elemental-blocks:javascript/media-gallery-element.js');
    }
}

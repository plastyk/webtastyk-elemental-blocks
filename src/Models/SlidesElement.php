<?php

namespace Plastyk\Elemental\Models;

use Colymba\BulkUpload\BulkUploader;
use DNADesign\Elemental\Models\BaseElement;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\Image;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
use SilverStripe\ORM\DataList;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;

/**
 * @method Image StaticBackground()
 * @method DataList Slides()
 */
class SlidesElement extends BaseElement
{
    private static $table_name = 'SlidesElement';
    private static $singular_name = 'Slides';
    private static $plural_name = 'Slides';
    private static $description = 'A set of images, text, and optionally CTA.';
    private static $inline_editable = false;

    private static $has_one = [
        'StaticBackground' => Image::class,
    ];

    private static $has_many = [
        'Slides' => Slide::class,
    ];

    private static $owns = [
        'StaticBackground',
        'Slides',
    ];

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->removeByName('Slides');
        $fields->removeByName('TitleAndDisplayed');

        $fields->addFieldToTab(
            'Root.Main',
            UploadField::create('StaticBackground', 'Static Background')
                ->setFolderName('Uploads/Images')
        );

        $slidesFieldConfig = GridFieldConfig_RecordEditor::create();
        $slidesFieldConfig->addComponent(GridFieldOrderableRows::create('SortOrder'));

        // $slidesFieldBulkUpload = new BulkUploader();
        // $slidesFieldBulkUpload->setUfSetup('setFolderName', 'Uploads/Images');
        // $slidesFieldConfig->addComponent($slidesFieldBulkUpload);

        $slidesField = GridField::create(
            'Slides',
            '',
            $this->Slides(),
            $slidesFieldConfig
        );
        $fields->addFieldToTab('Root.Main', $slidesField);

        return $fields;
    }

    public function getType()
    {
        return static::$singular_name;
    }
}

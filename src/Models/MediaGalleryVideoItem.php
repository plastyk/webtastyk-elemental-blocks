<?php

namespace Plastyk\Elemental\Models;

use SilverStripe\Core\Injector\Injector;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\OptionsetField;
use SilverStripe\ORM\DataObject;
use Symfony\Component\Cache\Simple\FilesystemCache;
use SilverStripe\ORM\FieldType\DBVarchar;
use SilverStripe\ORM\FieldType\DBEnum;

/**
 * @property string $Title Varchar(255)
 * @property string $VideoID Varchar(64)
 * @property string $VideoType Enum("YouTube,Vimeo", "YouTube")
 * @property int $SortOrder
 * @property bool $Active
 *
 * @method VideoGalleryElement Element()
 */
class MediaGalleryVideoItem extends MediaGalleryItem
{
    private static $table_name = 'MediaGalleryVideoItem';
    private static $singular_name = 'Video';
    private static $plural_name = 'Videos';

    private static $db = [
        'VideoID' => DBVarchar::class . '(64)',
        'VideoType' => DBEnum::class . '("YouTube,Vimeo", "YouTube")',
    ];

    private static $field_labels = [
        'VideoID' => 'Video ID',
        'VideoType' => 'Video Type',
    ];

    private static $defaults = [
        'VideoType' => 'YouTube',
    ];

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->removeByName('LinkTracking');
        $fields->removeByName('FileTracking');

        $fields->removeFieldFromTab('Root.Main', 'SortOrder');
        $fields->removeFieldFromTab('Root.Main', 'ElementID');

        $fields->addFieldToTab('Root.Main', OptionsetField::create('VideoType', 'Video Type', $this->dbObject('VideoType')->enumValues()), 'VideoID');

        if ($this->VideoID && $this->VideoEmbedLink()) {
            $fields->addFieldToTab('Root.Main', LiteralField::create('VideoPreview', '<iframe src="' . $this->VideoEmbedLink() . '" width="320" height="180"></iframe>'));
        }

        return $fields;
    }

    public function IsActive() {
        return $this->Active && $this->VideoID !== null;
    }

    public function Type() {
        return $this->VideoType;
    }

    public function VideoImage()
    {
        $videoPlaceholderThumbnail = 'public/resources/vendor/plastyk/webtastyk-elemental-blocks/images/video-placeholder.jpg';

        if (!$this->VideoID) {
            return $videoPlaceholderThumbnail;
        }

        if ($this->VideoType == 'YouTube') {
            return 'https://i3.ytimg.com/vi/' . $this->VideoID . '/mqdefault.jpg';
        }

        if ($this->VideoType == 'Vimeo') {
            $cache = Injector::inst()->get(FilesystemCache::class . 'Video_Gallery');
            $videoThumbnail = $cache->get('Video_Gallery_Video_Thumbnail_' . $this->VideoID);

            if ($videoThumbnail) {
                return $videoPlaceholderThumbnail;
            }

            $curlRequest = curl_init();
            curl_setopt($curlRequest, CURLOPT_URL, 'https://vimeo.com/api/v2/video/' . $this->VideoID . '.php');
            curl_setopt($curlRequest, CURLOPT_RETURNTRANSFER, 1);
            $curlHTML = curl_exec($curlRequest);
            $curlCode = curl_getinfo($curlRequest, CURLINFO_HTTP_CODE);
            curl_close($curlRequest);

            if ($curlCode != 200) {
                return false;
            }

            $vimeoVideo = unserialize($curlHTML);
            $videoThumbnail = $vimeoVideo[0]['thumbnail_large'];

            $cache->set('Video_Gallery_Video_Thumbnail_' . $this->VideoID, $videoThumbnail);

            return $videoThumbnail;
        }

        return $videoPlaceholderThumbnail;
    }

    public function VideoLink()
    {
        if (!$this->VideoID) {
            return false;
        }

        if ($this->VideoType == 'YouTube') {
            return 'https://www.youtube.com/watch?v=' . $this->VideoID;
        }

        if ($this->VideoType == 'Vimeo') {
            return 'https://player.vimeo.com/video/' . $this->VideoID;
        }

        return false;
    }

    public function VideoEmbedLink()
    {
        if (!$this->VideoID) {
            return false;
        }

        if ($this->VideoType == 'YouTube') {
            return 'https://www.youtube.com/embed/' . $this->VideoID;
        }

        if ($this->VideoType == 'Vimeo') {
            return 'https://player.vimeo.com/video/' . $this->VideoID;
        }

        return false;
    }
}

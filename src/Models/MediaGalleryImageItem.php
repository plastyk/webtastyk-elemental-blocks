<?php

namespace Plastyk\Elemental\Models;

use SilverStripe\Assets\Image;

/**
 * @property string $Title Varchar(255)
 * @property string $ImageID Varchar(64)
 * @property string $ImageType Enum("YouTube,Vimeo", "YouTube")
 * @property int $SortOrder
 * @property bool $Active
 */
class MediaGalleryImageItem extends MediaGalleryItem
{
    private static $table_name = 'MediaGalleryImageItem';
    private static $singular_name = 'Image';
    private static $plural_name = 'Images';

    private static $has_one = [
        'Image' => Image::class,
    ];

    private static $owns = [
        'Image',
    ];

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->removeByName('LinkTracking');

        $fields->removeFieldFromTab('Root.Main', 'SortOrder');
        $fields->removeFieldFromTab('Root.Main', 'ElementID');

        if ($imageField = $fields->fieldByName('Root.Main.Image')) {
            $imageField
                ->setFolderName('Uploads/Images')
                ->setDescription('Thumbnail will have a 16:9 aspect ratio');
        }

        return $fields;
    }

    public function IsActive()
    {
        return $this->Active && $this->Image()->exists();
    }

    public function Type()
    {
        return 'Image';
    }
}

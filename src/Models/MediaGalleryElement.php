<?php

namespace Plastyk\Elemental\Models;

use DNADesign\Elemental\Models\BaseElement;
use Plastyk\Elemental\Control\MediaGalleryElementController;
use SilverStripe\Core\ClassInfo;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldAddNewButton;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
use SilverStripe\ORM\DataList;
use Symbiote\GridFieldExtensions\GridFieldAddNewMultiClass;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;

/**
 * @method DataList GalleryItems()
 */
class MediaGalleryElement extends BaseElement
{
    private static $table_name = 'GalleryElement';
    private static $singular_name = 'Media Gallery';
    private static $plural_name = 'Media Galleries';
    private static $description = 'A row of images and videos that open in a popup when clicked';
    private static $controller_class = MediaGalleryElementController::class;
    private static $inline_editable = false;

    private static $has_many = [
        'GalleryItems' => MediaGalleryItem::class,
    ];

    private static $owns = [
        'GalleryItems',
    ];

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->removeByName('GalleryItems');
        $fields->removeByName('LinkTracking');
        $fields->removeByName('FileTracking');

        $fields->removeByName('TitleAndDisplayed');

        $itemsFieldConfig = GridFieldConfig_RecordEditor::create();

        $multiAdd = new GridFieldAddNewMultiClass();

        $multiAddClasses = ClassInfo::subclassesFor(MediaGalleryItem::class);
        unset($multiAddClasses[array_search(MediaGalleryItem::class, $multiAddClasses)]);
        sort($multiAddClasses);
        $multiAdd->setClasses($multiAddClasses);

        $itemsFieldConfig
            ->removeComponentsByType(GridFieldAddNewButton::class)
            ->addComponent($multiAdd)
            ->addComponent(GridFieldOrderableRows::create('SortOrder'));

        $itemsField = GridField::create(
            'GalleryItems',
            'Items',
            $this->GalleryItems(),
            $itemsFieldConfig
        );
        $fields->addFieldToTab('Root.Main', $itemsField);

        return $fields;
    }

    public function getType()
    {
        return static::$singular_name;
    }

    public function ActiveGalleryItems()
    {
        return $this->GalleryItems()->filterByCallback(function ($item) {
            return $item->IsActive();
        });
    }
}

<?php

namespace Plastyk\Elemental\Models;

use DNADesign\Elemental\Models\BaseElement;
use SilverStripe\ORM\FieldType\DBText;

/**
 * @property string $Content
 *
 * @method DataList Links()
 */
class CallToActionElement extends BaseElement
{
    private static $table_name = 'CallToActionElement';
    private static $singular_name = 'Call To Action';
    private static $plural_name = 'Calls To Action';
    private static $description = 'A short description adjacent to a Call-To-Action Button';
    private static $inline_editable = false;

    private static $db = [
        'Content' => DBText::class,
    ];

    private static $has_many = [
        'Links' => CallToActionLink::class,
    ];

    private static $summary_fields = [
        'Content',
    ];

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->removeFieldFromTab('Root.Main', 'SortOrder');
        $fields->removeFieldFromTab('Root.Main', 'ElementID');


        $links = $fields->fieldByName('Root.Links.Links');
        if ($links) {
            $fields->removeByName('Links');
            $fields->addFieldToTab('Root.Main', $links);
        }

        return $fields;
    }

    public function getType()
    {
        return static::$singular_name;
    }
}

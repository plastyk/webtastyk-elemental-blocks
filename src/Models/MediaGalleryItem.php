<?php

namespace Plastyk\Elemental\Models;

use SilverStripe\ORM\DataObject;
use SilverStripe\ORM\FieldType\DBBoolean;
use SilverStripe\ORM\FieldType\DBInt;
use SilverStripe\ORM\FieldType\DBVarchar;

/**
 * @property string Title
 * @property int SortOrder
 * @property bool Active
 * @property int ElementID
 *
 * @method MediaGalleryElement Element()
 */
class MediaGalleryItem extends DataObject
{
    private static $table_name = 'GalleryItem';

    private static $db = [
        'Title' => DBVarchar::class,
        'SortOrder' => DBInt::class,
        'Active' => DBBoolean::class,
    ];

    private static $has_one = [
        'Element' => MediaGalleryElement::class,
    ];

    private static $field_labels = [
        'Active.Nice' => 'Active',
    ];

    private static $summary_fields = [
        'ImageThumbnail',
        'Title',
        'Type',
        'Active.Nice',
    ];

    private static $defaults = [
        'Active' => true,
    ];

    private static $searchable_fields = [
        'Title',
        'Active',
    ];

    private static $can_create = false;
    private static $default_sort = 'SortOrder ASC';

    private static $title_max_length = 255;


    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $titleMaxLength = intval(static::config()->get('title_max_length'));
        $fields->fieldByName('Root.Main.Title')->setMaxLength($titleMaxLength);

        return $fields;
    }

    public function forTemplate()
    {
        return $this->renderWith($this->getViewerTemplates());
    }

    public function IsActive()
    {
        return $this->Active;
    }

    public function Type()
    {
        return 'Media';
    }

    public function ImageThumbnail()
    {
        if ($image = $this->ImageID != '') {
            return $this->Image()->CMSThumbnail();
        }

        return '';
    }
}

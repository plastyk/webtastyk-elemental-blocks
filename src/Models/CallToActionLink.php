<?php

namespace Plastyk\Elemental\Models;

use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Forms\TreeDropdownField;
use SilverStripe\ORM\DataObject;
use SilverStripe\ORM\FieldType\DBInt;
use SilverStripe\ORM\FieldType\DBVarchar;

/**
 * @property string $LinkText
 * @property int $SortOrder
 * @property int $LinedPageID
 * @property int $ElementID
 *
 * @method SiteTree LinkedPage()
 * @method CallToActionElement Element()
 */
class CallToActionLink extends DataObject
{
    private static $table_name = 'CallToActionLink';

    private static $db = [
        'LinkText' => DBVarchar::class,
        'SortOrder' => DBInt::class,
    ];

    private static $has_one = [
        'LinkedPage' => SiteTree::class,
        'Element' => CallToActionElement::class,
    ];

    private static $summary_fields = [
        'LinkText',
        'LinkedPage.Title' => 'Linked Page',
    ];

    private static $link_text_max_length = 18;

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->removeFieldFromTab('Root.Main', 'SortOrder');
        $fields->removeFieldFromTab('Root.Main', 'ElementID');

        $linkTextMaxLength = intval(static::config()->get('link_text_max_length'));
        $fields->fieldByName('Root.Main.LinkText')->setMaxLength($linkTextMaxLength);
        $fields->addFieldToTab('Root.Main', TreeDropdownField::create('LinkedPageID', 'Choose a page to link to:', SiteTree::class));

        return $fields;
    }

    public function getLinkText()
    {
        return $this->getField('LinkText') ?: $this->LinkedPage()->Title;
    }
}

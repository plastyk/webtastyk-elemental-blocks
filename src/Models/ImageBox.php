<?php

namespace Plastyk\Elemental\Models;

use SilverStripe\Assets\Image;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Forms\TreeDropdownField;
use SilverStripe\ORM\DataObject;
use SilverStripe\ORM\FieldType\DBInt;
use SilverStripe\ORM\FieldType\DBText;
use SilverStripe\Versioned\Versioned;

/**
 * @var string $Content
 * @var int $SortOrder
 * @var int $LinkedPageID
 * @var int $ImageID
 * @var int $ElementID
 *
 * @method SiteTree LinkedPage()
 * @method Image Image()
 * @method ImageBoxesElement Element()
 */
class ImageBox extends DataObject
{
    private static $table_name = 'ImageBox';
    private static $singular_name = 'Image Box';
    private static $plural_name = 'Image Boxes';

    private static $db = [
        'Content' => DBText::class,
        'SortOrder' => DBInt::class,
    ];

    private static $has_one = [
        'LinkedPage' => SiteTree::class,
        'Image' => Image::class,
        'Element' => ImageBoxesElement::class,
    ];

    private static $owns = [
        'Image',
    ];

    private static $summary_fields = [
        'Content',
        'Image.CMSThumbnail',
    ];

    private static $field_labels = [
        'Image.CMSThumbnail' => 'Image',
    ];

    private static $extensions = [
        Versioned::class . '.versioned',
    ];

    private static $default_sort = 'SortOrder ASC';


    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->removeFieldFromTab('Root.Main', 'SortOrder');
        $fields->removeFieldFromTab('Root.Main', 'ElementID');

        if ($imageField = $fields->fieldByName('Root.Main.Image')) {
            $imageField
                ->setFolderName('Uploads/Images')
                ->setDescription('3:2 aspect ratio. Minimum recommended size 900x600');
        }

        $fields->addFieldToTab('Root.Main', TreeDropdownField::create('LinkedPageID', 'Choose a page to link to:', SiteTree::class));

        return $fields;
    }
}

<?php

namespace Plastyk\Elemental\Models;

use SilverStripe\Assets\Image;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\TreeDropdownField;
use SilverStripe\ORM\DataObject;
use SilverStripe\ORM\FieldType\DBInt;
use SilverStripe\ORM\FieldType\DBText;
use SilverStripe\ORM\FieldType\DBVarchar;
use SilverStripe\Versioned\Versioned;

/**
 * @property string $Title Varchar(1024)
 * @property string $Content HTMLText
 * @property string $LinkText Varchar(1024)
 * @property int $SortOrder
 * @property int $LinkedPageID
 * @property int $ImageID
 * @property int $ElementID
 *
 * @method SiteTree LinkedPage()
 * @method Image Image()
 * @method SlidesElement Element()
 */
class Slide extends DataObject
{
    private static $table_name = 'Slide';

    private static $db = [
        'Title' => DBVarchar::class,
        'Content' => DBText::class,
        'LinkText' => DBVarchar::class,
        'SortOrder' => DBInt::class,
    ];

    private static $has_one = [
        'LinkedPage' => SiteTree::class,
        'Image' => Image::class,
        'Element' => SlidesElement::class,
    ];

    private static $owns = [
        'Image',
    ];

    private static $summary_fields = [
        'Title',
        'Image.CMSThumbnail',
    ];

    private static $field_labels = [
        'Image.CMSThumbnail' => 'Image',
    ];

    private static $extensions = [
        Versioned::class . '.versioned',
    ];

    private static $default_sort = 'SortOrder ASC';
    private static $singular_name = 'Slide';
    private static $plural_name = 'Slides';

    private static $title_max_length = 255;
    private static $link_text_max_length = 255;

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->removeFieldFromTab('Root.Main', 'SortOrder');
        $fields->removeFieldFromTab('Root.Main', 'ElementID');

        if ($imageField = $fields->fieldByName('Root.Main.Image')) {
            $imageField
                ->setFolderName('Uploads/Images')
                ->setDescription('3:2 aspect ratio. Minimum recommended size 900x600');
        }

        $config = static::config();

        $titleMaxLength = intval($config->get('title_max_length'));
        $fields->fieldByName('Root.Main.Title')->setMaxLength($titleMaxLength);

        $fields->addFieldToTab('Root.Main', TreeDropdownField::create('LinkedPageID', 'Choose a page to link to:', SiteTree::class));

        $linkTextMaxLength = intval($config->get('link_text_max_length'));
        $fields->addFieldToTab(
            'Root.Main',
            TextField::create('LinkText', 'Call to Action Text')
                ->setMaxLength($linkTextMaxLength)
        );

        return $fields;
    }
}

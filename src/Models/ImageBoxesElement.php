<?php

namespace Plastyk\Elemental\Models;

use Colymba\BulkUpload\BulkUploader;
use DNADesign\Elemental\Models\BaseElement;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;

/**
 * @method DataList Boxes()
 */
class ImageBoxesElement extends BaseElement
{
    private static $table_name = 'ImageBoxesElement';
    private static $singular_name = 'Image Boxes';
    private static $plural_name = 'Image Boxes';
    private static $description = 'A set of images, text, and optionally CTA.';
    private static $inline_editable = false;

    private static $has_many = [
        'Boxes' => ImageBox::class,
    ];

    private static $owns = [
        'Boxes',
    ];

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->removeByName('Boxes');
        $fields->removeByName('TitleAndDisplayed');

        $boxesFieldConfig = GridFieldConfig_RecordEditor::create();
        $boxesFieldConfig->addComponent(GridFieldOrderableRows::create('SortOrder'));

        // $boxesFieldBulkUpload = new BulkUploader();
        // $boxesFieldBulkUpload->setUfSetup('setFolderName', 'Uploads/Images');
        // $boxesFieldConfig->addComponent($boxesFieldBulkUpload);

        $boxesField = GridField::create(
            'Boxes',
            '',
            $this->Boxes(),
            $boxesFieldConfig
        );
        $fields->addFieldToTab('Root.Main', $boxesField);

        return $fields;
    }

    public function getType()
    {
        return static::$singular_name;
    }
}

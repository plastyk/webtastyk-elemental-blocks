<% if $Boxes %>
<div class="image-boxes-element container-fluid no-outer-gutters<% if $Style %> $StyleVariant<% end_if %>">
	<div class="row">
	<% loop $Boxes.Limit(3) %>
		<div class="image-boxes-element__box col col-12 col-sm-4">
			<a class="image-boxes-element__box__link" href="$LinkedPage.Link.ATT">
				<% if $Image %>
				<img class="image-boxes-element__box__image" src="$Image.FocusFill(500,300).URL" alt="$Image.Title">
				<% end_if %>

				<% if $Content %>
				<div class="image-boxes-element__box__content">
					$Content
				</div>
				<% end_if %>
			</a>
		</div>
	<% end_loop %>
	</div>
</div>
<% end_if %>

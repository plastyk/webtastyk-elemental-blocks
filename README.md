# Webtastyk elements

Temporary elements module for common webtastyk elements. This module will be deprecated in place of individually versioned element modules.

## Configuration

Some text lengths are configurable with the following settings (and defaults):

<!-- prettier-ignore -->
```yml
Plastyk\Elemental\Models\CallToActionLink:
  link_text_max_length: 18

Plastyk\Elemental\Models\MediaGalleryItem:
  title_max_length: 255

Plastyk\Elemental\Models\Slide:
  title_max_length: 255
  link_text_max_length: 255
```

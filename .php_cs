<?php

$userConfig = $_SERVER['HOME'] . DIRECTORY_SEPARATOR . '.php_cs';
if (is_file($userConfig)) {
    echo 'Loading ' . $userConfig . "\n";
    $fixer = require $userConfig;
    $fixer->getRules()['array_syntax'] = [
        'syntax' => 'short',
    ];
} else {
    $fixer = PhpCsFixer\Config::create()
        ->setRules([
            '@PSR1' => true,
            '@PSR2' => true,
            'array_syntax' => [
                'syntax' => 'short',
            ],
        ]);
}

$fixer
    ->setUsingCache(false)
    ->getFinder()
    ->in(['src'])
    ->append(['_config.php']);

return $fixer;

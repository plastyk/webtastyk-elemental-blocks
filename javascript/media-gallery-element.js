(function($) {
	$(document).ready(function() {
		$('.media-gallery-box--video .media-gallery-box__link').magnificPopup({
			disableOn: 700,
			type: 'iframe',
			mainClass: 'mfp-fade',
			removalDelay: 160,
			preloader: false,
			fixedContentPos: false
		});
		$('.media-gallery-box--image .media-gallery-box__link').magnificPopup({
			disableOn: 700,
			type: 'image',
			mainClass: 'mfp-fade',
			removalDelay: 160,
			preloader: false,
			fixedContentPos: false
		});
	});
})(jQuery);
